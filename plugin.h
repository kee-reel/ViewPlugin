#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Architecture/GUIElementBase/guielementbase.h"

#include "../../Interfaces/Utility/ilogicplugin.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.ViewPlugin" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	GUIElementBase* m_guiElementBase;
	ReferenceInstancePtr<ILogicPlugin> m_logicPlugin;
};
